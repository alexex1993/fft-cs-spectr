﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Microsoft.DirectX.DirectSound;
using Microsoft.Win32.SafeHandles;

namespace SoundFiltering
{

    public class SoundSource : IDisposable
    {
        public delegate void ProcessData_Delegate(float [] data);

        public event ProcessData_Delegate ProcessData;

        public event ProcessData_Delegate ProcessFFTData;

        protected object m_disposeLock = new object();

        protected bool m_disposed = false;

        protected Capture m_capture = null;

        protected CaptureBuffer m_buffer = null;

        protected Thread m_captureThread = null;

        protected int m_captureBufferLength = 0;

        protected Notify m_notify;

        protected AutoResetEvent m_bufferPositionEvent = null;

        protected SafeWaitHandle m_bufferPositionEvent_Handle = null;

        protected ManualResetEvent m_threadTerminateEvent = null;

        const int c_bufferTimeInSeconds = 5;

        const int c_notifiesInSeconds = 2;

        public Guid Guid { get; set; }

        public string Description { get; set; }

        public bool IsDefault
        {
            get { return Guid == Guid.Empty; }
        }

        public bool IsCapturing { get; protected set; }

        public int SampleRateHz { get; set; }

        public SoundSource(Guid id, string name)
        {
            Guid = id;
            Description = name;
        }

        public SoundSource()
        {
            CaptureDevicesCollection cdc = new CaptureDevicesCollection();
            for (int i = 0; i < cdc.Count; i++)
            {
                if (cdc[i].DriverGuid == Guid.Empty)
                {
                    Guid = cdc[i].DriverGuid;
                    Description = cdc[i].Description;
                    break;
                }
            }

            m_bufferPositionEvent = new AutoResetEvent(false);
            m_bufferPositionEvent_Handle = m_bufferPositionEvent.SafeWaitHandle;
            m_threadTerminateEvent = new ManualResetEvent(true);
        }

        void IDisposable.Dispose()
        {
            Dispose(true);
        }

        ~SoundSource()
        {
            Dispose(false);
        }

        public void Dispose(bool disposing)
        {
            lock (m_disposeLock)
            {
                if (m_disposed) return;
                m_disposed = true;
            }
            GC.SuppressFinalize(this);
            if (IsCapturing)
            {
                Stop();
            }
            if (m_bufferPositionEvent_Handle != null)
            {
                m_bufferPositionEvent_Handle.Dispose();
            }
            if (m_bufferPositionEvent != null)
            {
                m_bufferPositionEvent.Close();
            }
            if (m_threadTerminateEvent != null)
            {
                m_threadTerminateEvent.Close();
            }
        }

        public static List<SoundSource> GetSources()
        {
            CaptureDevicesCollection captureDevices = new CaptureDevicesCollection();
            List<SoundSource> deviceList = new List<SoundSource>();
            foreach (DeviceInformation captureDevice in captureDevices)
            {
                deviceList.Add(new SoundSource(captureDevice.DriverGuid, captureDevice.Description));
            }
            return deviceList;
        }

        public void Start()
        {
            if (IsCapturing)
            {
                return;
            }
            IsCapturing = true;

            m_capture = new Capture(Guid);
            WaveFormat waveFormat = new WaveFormat();

            
            waveFormat.Channels = (short)m_capture.Caps.Channels; 
            waveFormat.BitsPerSample = 16; 
            waveFormat.SamplesPerSecond = 44000;
            waveFormat.FormatTag = WaveFormatTag.Pcm;
            waveFormat.BlockAlign = (short)((waveFormat.Channels * waveFormat.BitsPerSample + 7) / 8);
            waveFormat.AverageBytesPerSecond = waveFormat.BlockAlign * waveFormat.SamplesPerSecond;

            m_captureBufferLength = waveFormat.AverageBytesPerSecond * c_bufferTimeInSeconds;
            CaptureBufferDescription captureBufferDescription = new CaptureBufferDescription();
            captureBufferDescription.Format = waveFormat;
            captureBufferDescription.BufferBytes = m_captureBufferLength;
            m_buffer = new CaptureBuffer(captureBufferDescription, m_capture);

            int waitHandleCount = c_bufferTimeInSeconds * c_notifiesInSeconds;
            BufferPositionNotify[] positions = new BufferPositionNotify[waitHandleCount];
            for (int i = 0; i < waitHandleCount; i++)
            {
                BufferPositionNotify position = new BufferPositionNotify();
                position.Offset = (i + 1) * m_captureBufferLength / positions.Length - 1;
                position.EventNotifyHandle = m_bufferPositionEvent_Handle.DangerousGetHandle();
                positions[i] = position;
            }

            m_notify = new Notify(m_buffer);
            m_notify.SetNotificationPositions(positions);

            m_threadTerminateEvent.Reset();
            m_captureThread = new Thread(new ThreadStart(ThreadLoop));
            m_captureThread.Name = "Sound capture";
            m_captureThread.Start();
        }

        public void Stop()
        {
            if (IsCapturing)
            {
                IsCapturing = false;

                m_threadTerminateEvent.Set();
                m_captureThread.Join();

                m_notify.Dispose();
                m_buffer.Dispose();
                m_capture.Dispose();
            }
        }

        #region Worker protected stuff.
        private void ThreadLoop()
        {
            m_buffer.Start(true);
            try
            {
                int nextCapturePosition = 0;
                WaitHandle[] handles = new WaitHandle[] {m_threadTerminateEvent, m_bufferPositionEvent };
                while (WaitHandle.WaitAny(handles) > 0)
                {
                    int capturePosition, readPosition;
                    m_buffer.GetCurrentPosition(out capturePosition, out readPosition);

                    int lockSize = readPosition - nextCapturePosition;
                    if (lockSize < 0) lockSize += m_captureBufferLength;
                    if ((lockSize & 1) != 0) lockSize--;

                    int itemsCount = lockSize >> 1;

                    short[] data = (short[])m_buffer.Read(nextCapturePosition, typeof(short), LockFlag.None, itemsCount);
                    processData(data);
                    nextCapturePosition = (nextCapturePosition + lockSize) % m_captureBufferLength;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                m_buffer.Stop();
            }
        }

        protected void processData(short[] data)
        {
            ProcessData_Delegate pdt = ProcessData;
            ProcessData_Delegate pfd = ProcessFFTData;
            if ((pdt == null) && (pfd == null)) return;

            int size = 2;
            while (size < data.Length)
            {
                size = size * 2;
            }
            float[] xf = new float[size];
            float[] xt = new float[data.Length];
            for (int i = 0; i < data.Length; i++)
            {
                float v = (float)data[i];
                xf[i] = v;
                xt[i] = v;
            }
            for (int i = data.Length; i < size; i++)
            {
                xf[i] = 0;
            }
       
            if (pdt != null)
            {
                pdt(xt);
            }
            if (pfd != null)
            {
                float[] fft = new float[xf.Length];
                Math.FFT.Forward(xf, fft);
                pfd(fft);
            }
        }
        #endregion
    }
}
