﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.DirectX.DirectSound;

namespace SoundFiltering
{
    public class SoundPlayback : IDisposable
    {
        protected Device m_src = null;

        protected SecondaryBuffer m_secondaryBuffer = null;

        protected int Location = 0;

        protected bool m_disposed = false;

        void IDisposable.Dispose()
        {
            Dispose(true);
        }

        ~SoundPlayback()
        {
            Dispose(false);
        }

        public void Dispose(bool disposing)
        {
            if (m_disposed) return;
            m_disposed = true;
            
            GC.SuppressFinalize(this);

            m_secondaryBuffer.Dispose();
            m_src.Dispose();
        }

        public void Setup(System.Windows.Forms.Control owner, string fileName)
        {
            m_src = new Device();
            m_src.SetCooperativeLevel(owner, CooperativeLevel.Priority);
            m_secondaryBuffer = new SecondaryBuffer(fileName, m_src);
            m_secondaryBuffer.SetCurrentPosition(0);
        }

        public bool GetData(float[] buffer)
        {
            try
            {
                int bits = m_secondaryBuffer.Format.BitsPerSample;
                byte[] buf = new byte[buffer.Length * bits / 8];
                System.IO.MemoryStream memStream = new System.IO.MemoryStream(buf);
                memStream.Position = 0;
                //  memStream.Read(m_secondaryBuffer, 0, buffer.Length);
                m_secondaryBuffer.Read(Location, memStream, buffer.Length, LockFlag.None);
                Location += buffer.Length;
                int k = 0;
                if (bits == 16)
                {
                    for (int i = 0; i < buffer.Length; i++)
                    {
                        int l = buf[k++];
                        int u = buf[k++];
                        buffer[i] = (float)(l + (u * 256));
                    }
                }
                else
                {
                    for (int i = 0; i < buffer.Length; i++)
                    {
                        buffer[i] = (float)(buf[k++]);
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
    }
}
