﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SoundFiltering
{

    public partial class Form1 : Form
    {
        protected SoundSource source = null;
        protected SoundPlayback playback = null;

        public Form1()
        {
            InitializeComponent();
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            if (source != null)
            {
                source.Dispose(true);
            }
            if (playback != null)
            {
                playback.Dispose(true);
            }
        }


        private void button1_Click(object sender, EventArgs e)
        {
            if (source != null) return;
            source = new SoundSource();
            source.ProcessFFTData += new SoundSource.ProcessData_Delegate(source_ProcessFFTData);
            source.ProcessData += new SoundSource.ProcessData_Delegate(source_ProcessData);
            source.Start();
        }

        void source_ProcessData(float[] data)
        {
            soundVisualizer1.AppendData(data);
            soundVisualizer1.TriggerRedraw();
        }

        void source_ProcessFFTData(float[] data)
        {
            float[] tmp = new float[data.Length / 2];
            for (int i = 0; i < tmp.Length; i++) tmp[i] = data[i];
            if (graph1.Lines.Count < 1)
            {
                graph1.Lines.Add(new Graph.Line());
            }
            graph1.Lines[0].Update(tmp);
            graph1.TriggerRedraw();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (playback == null)
            {
                playback = new SoundPlayback();
            }
            DialogResult dr = openFileDialog1.ShowDialog();
            if (!((dr == DialogResult.OK) || (dr == DialogResult.Yes))) return;
            playback.Setup(this, openFileDialog1.FileName);
            timer1.Enabled = true;
        }


        private void timer1_Tick(object sender, EventArgs e)
        {
            float[] buff = new float[2048];
            if (playback.GetData(buff) == false)
            {
                timer1.Enabled = false;
                return;
            }
     
            source_ProcessData(buff);

            float[] fft = new float[buff.Length];
            Math.FFT.Forward(buff, fft);
            source_ProcessFFTData(fft);
            source_ProcessData(fft);
        }
    }
}
