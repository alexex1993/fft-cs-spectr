﻿namespace Math
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public class FFT
    {
        public static void Forward(double[] reals)
        {
            doFFT(reals, true);
        }

        public static void Forward(double[] realIn, double[] realOut)
        {
            doFFTReals(realIn, realOut, true);
        }

        public static void Forward(double[] realIn, double[] realOut, double[] imagOut)
        {
            doFFTReals(realIn, realOut, imagOut, true);
        }

        public static void Forward(double[] realIn, double [] imagIn, double[] realOut, double[] imagOut)
        {
            doFFT(realIn, imagIn, realOut, imagOut, true);
        }


        public static void Inverse(double[] reals)
        {
            doFFT(reals, false);
        }

        public static void Inverse(double[] realIn, double[] realOut)
        {
            doFFTReals(realIn, realOut, false);
        }

        public static void Inverse(double[] realIn, double[] realOut, double[] imagOut)
        {
            doFFTReals(realIn, realOut, imagOut, true);
        }

        public static void Inverse(double[] realIn, double[] imagIn, double[] realOut, double[] imagOut)
        {
            doFFT(realIn, imagIn, realOut, imagOut, false);
        }

        public static void Forward(float[] reals)
        {
            doFFT(reals, true);
        }

        public static void Forward(float[] realIn, float[] magOut)
        {
            doFFTReals(realIn, magOut, true);
        }

        public static void Forward(float[] realIn, float[] imagIn, float[] realOut)
        {
            doFFTReals(realIn, imagIn, realOut, true);
        }

        public static void Forward(float[] realIn, float[] imagIn, float[] realOut, float[] imagOut)
        {
            doFFT(realIn, imagIn, realOut, imagOut, true);
        }

        public static void Inverse(float[] reals)
        {
            doFFT(reals, false);
        }

        public static void Inverse(float[] realIn, float[] realOut)
        {
            doFFTReals(realIn, realOut, false);
        }

        public static void Inverse(float[] realIn, float[] realOut, float[] imagOut)
        {
            doFFTReals(realIn, realOut, imagOut, true);
        }

        public static void Inverse(float[] realIn, float[] imagIn, float[] realOut, float[] imagOut)
        {
            doFFT(realIn, imagIn, realOut, imagOut, false);
        }


        protected static double[] m_sinDouble = null;

        protected static float[] m_sinFloat = null;

        /// Table is sin(pi/2), sin(pi/4), sin(pi/8) ...

        protected static double[] m_cosDouble = null;

        protected static float[] m_cosFloat = null;

        protected static object m_tableLock = new object();
      
        protected static void MakeTables()
        {

            if (m_sinDouble != null) return;
            lock (m_tableLock)
            {
                if (m_sinDouble != null) return;
                int size = 20;
                double [] nsd = new double[size];
                float [] nsf = new float[size];
                double[] ncd = new double[size];
                float[] ncf = new float[size];
                double pi = System.Math.PI;

                for (int i = 0; i < size; i++)
                {
                    nsd[i] = System.Math.Sin(pi);
                    nsf[i] = (float)(nsd[i]);
                    ncd[i] = System.Math.Cos(pi);
                    ncf[i] = (float)(ncd[i]);
                    pi = pi / 2;
                }
                m_sinFloat = nsf;
                m_sinDouble = nsd;
                m_cosFloat = ncf;
                m_cosDouble = ncd;
            }
        }

 
        protected static void doFFT(
                double[] realIn, bool forward)
        {
            double[] realOut = new double[realIn.Length];
            double[] imagIn = new double[realIn.Length];
            double[] imagOut = new double[realIn.Length];
            for (int i = 0; i < realIn.Length; i++)
            {
                imagIn[i] = 0;
            }
            doFFT(realIn, imagIn, realOut, imagOut, forward);
            for (int i = 0; i < realIn.Length; i++)
            {
                realOut[i] = System.Math.Sqrt((realOut[i] * realOut[i]) + (imagOut[i] * imagOut[i]));
            }
        }

        protected static void doFFTReals(
                double[] realIn, double[] realOut, bool forward)
        {
            double[] imagIn = new double[realIn.Length];
            double[] imagOut = new double[realIn.Length];
            for (int i = 0; i < realIn.Length; i++)
            {
                imagIn[i] = 0;            
            }
            doFFT(realIn, imagIn, realOut, imagOut, forward);
            for (int i = 0; i < realIn.Length; i++)
            {
                realOut[i] = System.Math.Sqrt((realOut[i] * realOut[i]) + (imagOut[i] * imagOut[i]));
            }
        }


        protected static void doFFTReals(
                double[] realIn, double[] realOut, double [] imagOut, bool forward)
        {
            double[] imagIn = new double[realIn.Length];
            for (int i = 0; i < realIn.Length; i++)
            {
                imagIn[i] = 0;
            }
            doFFT(realIn, imagIn, realOut, imagOut, forward);
        }

        protected static void doFFT(
                double[] realIn, double[] imagIn, bool forward)
        {
            double[] realOut = new double[realIn.Length];
            double[] imagOut = new double[realIn.Length];
            for (int i = 0; i < realIn.Length; i++)
            {
                realOut[i] = realIn[i];
                imagOut[i] = imagIn[i];
            }
            doFFT(realOut, imagOut, realIn, imagIn, forward);
        }

        protected static void doFFT(
                double[] realIn, double[] imagIn,
                double[] realOut, double[] imagOut, bool forward)
        {
            MakeTables();
            int n = realIn.Length;
            int i;

            int bits = 0;
            for (i = 1; i < n; i <<= 1)
            {
                bits++;
            }

            for (i = 0; i < n; i++)
            {
                int p = bitrev(i, bits);
                realOut[p] = realIn[i];
                imagOut[p] = imagIn[i];
            }
            performFFT(realOut, imagOut, forward);
        }
        
        protected static int bitrev(int value, int bits)
        {
            // Reverse bits
            int rev = 0;
            for (int b = 0; b < bits; ++b)
            {
                rev <<= 1;
                rev |= value & 1;
                value >>= 1;
            }

            // Return reversed-bit value
            return rev;
        }

        protected static void performFFT(double[] r, double[] i, bool forward)
        {
            int len = r.Length;

            // Perform Cooley-Tukey fft
            double sign = forward ? 1 : -1;
            int index = 0;
            for (int p = 1; p < len; p <<= 1)
            {
                int p2 = p << 1;
                
                double sine = sign * m_sinDouble[index];
                double cosine = m_cosDouble[index++];

                // Seed the angle for the butterfly loops
                double wr = 1.0;
                double wi = 0.0;

                // Perform butterfly loops, just like most text books.
                int j;
                for (j = 0; j < p; ++j)
                {
                    int k;
                    for (k = j; k < len; k += p2)
                    {
                        int k2 = k + p;

                        // Perform butterfly
                        double tr = (wr * r[k2]) - (wi * i[k2]);
                        double ti = (wr * i[k2]) + (wi * r[k2]);
                        r[k2] = r[k] - tr;
                        i[k2] = i[k] - ti;
                        r[k] += tr;
                        i[k] += ti;
                    }

                    // Advance angle
                    double nwr = (wr * cosine) - (wi * sine);
                    double nwi = (wi * cosine) + (wr * sine);
                    wr = nwr;
                    wi = nwi;
                }
            }
        }


        protected static void doFFT(
                float[] realIn, bool forward)
        {
            float[] realOut = new float[realIn.Length];
            float[] imagIn = new float[realIn.Length];
            float[] imagOut = new float[realIn.Length];
            for (int i = 0; i < realIn.Length; i++)
            {
                imagIn[i] = 0;
            }
            doFFT(realIn, imagIn, realOut, imagOut, forward);
            for (int i = 0; i < realIn.Length; i++)
            {
                realOut[i] = (float)System.Math.Sqrt((realOut[i] * realOut[i]) + (imagOut[i] * imagOut[i]));
            }
        }

        protected static void doFFTReals(
                float[] realIn, float[] magOut, bool forward)
        {
            float[] imagIn = new float[realIn.Length];
            float[] imagOut = new float[realIn.Length];
            float[] realOut = new float[realIn.Length];
            for (int i = 0; i < realIn.Length; i++)
            {
                imagIn[i] = 0;
            }
            doFFT(realIn, imagIn, realOut, imagOut, forward);
            for (int i = 0; i < magOut.Length; i++)
            {
                magOut[i] = (float)System.Math.Sqrt((realOut[i] * realOut[i]) + (imagOut[i] * imagOut[i]));
            }
        }


        protected static void doFFTReals(
                float[] realIn, float[] imagIn, float[] realOut, bool forward)
        {
            float[] imagOut = new float[realIn.Length];
            doFFT(realIn, imagIn, realOut, imagOut, forward);
            for (int i = 0; i < realIn.Length; i++)
            {
                realOut[i] = (float)System.Math.Sqrt((realOut[i] * realOut[i]) + (imagOut[i] * imagOut[i]));
            }
        }

        protected static void doFFT(
                float[] realIn, float[] imagIn, bool forward)
        {
            float[] realOut = new float[realIn.Length];
            float[] imagOut = new float[realIn.Length];
            for (int i = 0; i < realIn.Length; i++)
            {
                realOut[i] = realIn[i];
                imagOut[i] = imagIn[i];
            }
            doFFT(realOut, imagOut, realIn, imagIn, forward);
        }

        protected static void doFFT(
                float[] realIn, float[] imagIn,
                float[] realOut, float[] imagOut, bool forward)
        {
            MakeTables();

            int n = realIn.Length;
            int i;

            // Calculate the number of bits required
            int bits = 0;
            for (i = 1; i < n; i <<= 1)
            {
                bits++;
            }

            n = 1 << (bits-1);
            // Copy bitreversed.
            for (i = 0; i < n; i++)
            {
                int p = bitrev(i, bits);
                realOut[p] = realIn[i];
                imagOut[p] = imagIn[i];
            }
            performFFT(realOut, imagOut, forward);
        }

        protected static void performFFT(float[] r, float[] i, bool forward)
        {
            int len = r.Length;

            // Perform Cooley-Tukey fft
            float sign = forward ? 1 : -1;
            int index = 0;
            for (int p = 1; p < len; p <<= 1)
            {
                int p2 = p << 1;

                // Calculate sine and cosine for angle advancing
                float sine = sign * m_sinFloat[index];
                float cosine = m_cosFloat[index++];

                // Seed the angle for the butterfly loops
                float wr = 1.0f;
                float wi = 0.0f;

                // Perform butterfly loops, just like most text books.
                int j;
                for (j = 0; j < p; j++)
                {
                    int k;
                    for (k = j; k < len; k += p2)
                    {
                        int k2 = k + p;

                        // Perform butterfly
                        float tr = (wr * r[k2]) - (wi * i[k2]);
                        float ti = (wr * i[k2]) + (wi * r[k2]);
                        r[k2] = r[k] - tr;
                        i[k2] = i[k] - ti;
                        r[k] += tr;
                        i[k] += ti;
                    }

                    // Advance angle
                    float nwr = (wr * cosine) - (wi * sine);
                    float nwi = (wi * cosine) + (wr * sine);
                    wr = nwr;
                    wi = nwi;
                }
            }
        }

    }
}
