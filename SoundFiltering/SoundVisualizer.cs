﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SoundFiltering
{
    /// <summary>
    /// Sound visualizer.
    /// </summary>
    public partial class SoundVisualizer : UserControl
    {
        protected bool m_close = false;

        protected Bitmap m_back = null;

        protected Bitmap m_front = null;

        protected List<Stack> m_stack = new List<Stack>();

        protected object m_lockObject = new object();

        protected float m_min = 0;

        protected float m_max = 0;

        protected List<float[]> m_toDoBuffers = new List<float[]>();

        protected object m_threadLock = new object();

        protected System.Threading.Thread m_workThread = null;

        public void TriggerRedraw()
        {
            timer1.Enabled = true;
        }

        public SoundVisualizer()
        {
            InitializeComponent();
            pictureBox1.Resize += new EventHandler(pictureBox1_Resize);
            this.Disposed += new EventHandler(SoundVisualizer_Disposed);
            ShowFFT = true;
            SpectralWidth = 256;
        }

        public void AppendData(float[] data)
        {
            lock (m_threadLock)
            {
                m_toDoBuffers.Add(data);
                if (m_workThread == null)
                {
                    m_workThread = new System.Threading.Thread(new System.Threading.ThreadStart(WorkLoop));
                    m_workThread.Name = "Sound visualization work thread.";
                    m_workThread.Start();
                }
            }
        }

        public int SpectralWidth { get; set; }

        public bool ShowFFT { get; set; }

        protected void SoundVisualizer_Disposed(object sender, EventArgs e)
        {
            m_close = true;
        }

        protected void pictureBox1_Resize(object sender, EventArgs e)
        {
            timer1.Enabled = true;
        }

        protected void resizeBuffers()
        {
            int w = pictureBox1.Width;
            int h = SpectralWidth/2; // pictureBox1.Height;
            w = w < 10 ? 10 : w;
            h = h < 10 ? 10 : h;
            System.Drawing.Bitmap old = m_back;
            if (old == null)
            {
                m_back = new Bitmap(w, h, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            }
            else
            {
                if ((m_back.Width != w) || (m_back.Height != h))
                {
                    m_back = new Bitmap(w, h, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
                }
            }
            if (m_back != old)
            {
                if (old != null)
                {
                    old.Dispose();
                }
            }
        }

        protected void swapBuffers()
        {
            System.Drawing.Bitmap tmp = m_front;
            m_front = m_back;
            m_back = tmp;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            doRedraw();
        }

        protected void doRedraw()
        {
            resizeBuffers();
            using (System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(m_back))
            {
                paintGraph(g, m_back, m_back.Width, m_back.Height);
            }
            swapBuffers();
            pictureBox1.Image = m_front;
        }

        protected class Stack
        {      
            protected static Color[] clrMap;

            protected static int []gamma = new int[1000];

            static Stack()
            {

                Color[] crs = {
                Color.Black, Color.Blue, Color.Green,
                Color.GreenYellow, Color.Yellow, Color.Orange, Color.OrangeRed, Color.Red};
                clrMap = new Color[crs.Length * 100];
                int k = 0;
                for (int i = 0; i < crs.Length - 1; i++)
                {
                    for (int j = 0; j < 100; j++)
                    {
                        Color low = crs[i];
                        Color high = crs[i + 1];
                        int r = ((low.R * (100 - j)) + (high.R * j)) / 100;
                        int g = ((low.G * (100 - j)) + (high.G * j)) / 100;
                        int b = ((low.B * (100 - j)) + (high.B * j)) / 100;
                        clrMap[k++] = Color.FromArgb(r, g, b);
                    }
                }
                float gammaCorrection = 0.3f;
                float gain = (float)((clrMap.Length - 1) / System.Math.Pow(gamma.Length, gammaCorrection));
                for (int i = 0; i < gamma.Length; i++)
                {
                    int val = (int)(gain * System.Math.Pow((double)i, gammaCorrection));
                    val = val < 0 ? 0 : val > clrMap.Length - 1 ? clrMap.Length - 1 : val;
                    gamma[i] = val;
                }
            }

            public Stack(float[] vals, double min, double max)
            {
                float scale = (float)(gamma.Length / (max - min));
                Colors = new Color[vals.Length / 2];
                for (int i = 0; i < Colors.Length; i++)
                {
                    int v = (int)((vals[i] - min) * scale);
                    v = v < 0 ? 0 : v > clrMap.Length - 1 ? clrMap.Length - 1 : v;
                    v = gamma[v];
                    Colors[i] = clrMap[v];
                }
            }

            public Color[] Colors = null;
        }

        protected void WorkLoop()
        {
            float[] lastBuf = null;
            while (m_close == false)
            {
                System.Threading.Thread.Sleep(100);
                float[] buf = null;
                lock (m_threadLock)
                {
                    if (m_toDoBuffers.Count > 0)
                    {
                        buf = m_toDoBuffers[0];
                        m_toDoBuffers.RemoveAt(0);
                    }
                }
                if (buf != null)
                {
                    if (lastBuf != null)
                    {
                        int sw = SpectralWidth;
                        float[] tmp = new float[(int)(sw * 1.75f)];
                        int k = 0; 
                        int swo = sw - (sw>>3);
                        int startLastBuf = lastBuf.Length - swo;
                        startLastBuf = startLastBuf < 0 ? 0 : startLastBuf;
                        for (int i = startLastBuf; i < lastBuf.Length; i++)
                        {
                            tmp[k++] = lastBuf[i];
                        }
                      
                        for (int i = 0; i < swo && k < tmp.Length && i < buf.Length; i++)
                        {
                            tmp[k++] = buf[i];
                        }
                        workOnBuffer(tmp);

                    }
                    workOnBuffer(buf);
                    
                    lastBuf = buf;
                }

            }
        }

        protected void workOnBuffer(float[] data)
        {
            List<Stack> tmp = new List<Stack>();

            int k = 0;
            float[] da = new float[SpectralWidth];
            float[] fft = new float[da.Length];
            int nextk = 0;
            while (k <= data.Length - da.Length)
            {
                nextk = k + (da.Length >> 3);
                for (int i = 0; i < da.Length; i++)
                {
                    da[i] = data[k++];
                }
                Math.FFT.Forward(da, fft);
                float[] outp = ShowFFT ? fft : da;
                float min = outp[0];
                float max = outp[0];
              
                for (int i = 0; i < outp.Length; i++)
                {
                    min = min < outp[i] ? min : outp[i];
                    max = max > outp[i] ? max : outp[i];
                }
                min = float.IsNaN(min) ? m_min : min;
                max = float.IsNaN(max) ? m_max : max;
                min = float.IsInfinity(min) ? m_min : min;
                max = float.IsInfinity(max) ? m_max : max;
                m_min = (255 * m_min + min) / 256;
                m_max = (255 * m_max + max) / 256;
                tmp.Add(new Stack(outp, m_min, m_max));
                k = nextk;
            }
            lock (m_lockObject)
            {
                m_stack.AddRange(tmp);
            }
        }
       
        protected void paintGraph(Graphics g, Bitmap b, int width, int height)
        {
            List<Stack> tmp = new List<Stack>();
            lock (m_lockObject)
            {
                if (m_stack.Count > 1)
                {
                    tmp.AddRange(m_stack);
                    m_stack.Clear();
                }
            }
            
            if (m_front == null)
            {
                g.Clear(Color.Black);
            }
            else
            {

                g.DrawImage(m_front, new Point(tmp.Count, 0));
                if (tmp.Count < 1)
                {
                    return;
                }
            }
            for (int i = 0; i < tmp.Count; i++)
            {
                int hh = tmp[0].Colors.Length; 
                for (int j = 0; j < height; j++)
                {
                    b.SetPixel(i, hh - j - 1, tmp[tmp.Count - 1 - i].Colors[j]);
                }
            }
        }
    }
}
